<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 14:37:17
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 14:51:15
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Custom.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\components;

use quick\admin\Element;

/**
 * 自定义组件
 * Class Custom
 * @package quick\components
 */
class Custom extends Element
{
    public $component;

    /**
     * Custom constructor.
     * @param $component
     */
    public function __construct($component)
    {
        $this->component = $component;
    }

    /**
     * Prepare the field for JSON serialization.
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(),[]);
    }
}