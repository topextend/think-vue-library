<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 14:23:11
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 14:48:36
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Row.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\components\layout;

use quick\admin\Element;

/**
 * Class Content
 */
class Row extends Element
{
    /**
     * @var string
     */
    public $component = "el-row";

    /**
     * Row constructor.
     * @param string|\Closure $content
     */
    public function __construct( $content = '')
    {
        if (!empty($content)) {
            $this->col(24, $content);
        }
    }

    /**
     * @param $width
     * @param $content
     * @return $this
     */
    public function col($width, $content)
    {
        $width = $width < 1 ? round(24 * $width) : $width;
        $col = Col::make($content, $width);
        return $this->addCol($col);
    }

    /**
     * @param Col $col
     * @return $this
     */
    protected function addCol(Col $col)
    {
        $this->children($col);
        return $this;
    }
}
