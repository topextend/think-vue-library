<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 14:28:05
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 14:52:14
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasSizeProps.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\components\metable;

trait HasSizeProps
{
    /**
     * @param string $size medium / small / mini
     * @return $this
     */
    public function size(string $size)
    {
        $this->attribute("size",$size);
        return $this;
    }
}