<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 13:31:23
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 13:35:28
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : QuickJobBase.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\library\queue\job;

use think\facade\Log;
use think\queue\Job;
use quick\admin\library\queue\job\QuickJobInterface;

class QuickJobBase implements QuickJobInterface
{
    /**
     * @var Job
     */
    public $job;

    /**
     * @param Job $job
     * @return $this
     */
    public function setJob(Job $job)
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @param $job
     * @return mixed|void
     */
   public function handel($job)
   {
       // TODO: Implement execute() method.
   }
}