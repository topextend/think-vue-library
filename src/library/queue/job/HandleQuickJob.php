<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 13:31:16
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 13:35:03
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HandleQuickJob.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\library\queue\job;

use quick\admin\library\queue\job\BaseJob;

class HandleQuickJob extends BaseJob
{
    /**
     * @param $job
     * @param array $data
     * @return bool|mixed
     */
    public function handle($job, array $data)
    {
        $command = unserialize($data['command']);
        return $this->app->invoke([$command, 'handle'],[$job]);
    }

    /**
     * @param array $data
     * @param $e
     */
    public function failed(array $data,$e)
    {
        if(!empty($data['data']['command'])){
            $command = unserialize($data['data']['command']);
            if (method_exists($command, 'failed')) {
                $command->failed();
            }
        }
    }
}