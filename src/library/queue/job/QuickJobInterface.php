<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 13:36:25
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 13:38:04
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : QuickJobInterface.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\library\queue\job;

use think\queue\Job;

/**
 * Interface QuickJobInterface
 * @package quick\admin\library\queue\job
 */
interface QuickJobInterface
{
    /**
     * @param $job
     * @return mixed
     */
    public function handel($job);
}