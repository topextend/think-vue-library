<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:03:23
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 16:43:20
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Like.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace quick\admin\filter\fields;

use think\helper\Arr;
use quick\admin\filter\fields\FieldFilter;

class Like extends FieldFilter
{
    /**
     * @var string
     */
    protected $exprFormat = '%{value}%';

    /**
     * @var string
     */
    protected $operator = 'like';

    /**
     * @param array $inputs
     * @return array|mixed|void|null
     */
    public function condition($inputs)
    {
        $value = Arr::get($inputs, $this->requestColumn);
        if (is_array($value)) {
            $value = array_filter($value);
        }
        if (is_null($value) || empty($value)) {
            return;
        }
        $this->value = $value;
        $expr = str_replace('{value}', $this->value, $this->exprFormat);
        return $this->buildCondition($this->column, $this->operator, $expr);
    }
}