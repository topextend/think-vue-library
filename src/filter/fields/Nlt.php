<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:03:48
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 16:46:47
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Nlt.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\filter\fields;

use think\helper\Arr;
use quick\admin\filter\fields\FieldFilter;

class Nlt extends FieldFilter
{
    public function condition($inputs)
    {
        $value = Arr::get($inputs, $this->requestColumn);
        if (is_null($value) || empty($value)) {
            return false;
        }
        $this->value = $value;
        return $this->buildCondition($this->column, '<=', $this->value);
    }
}