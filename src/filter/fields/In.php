<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:03:16
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 16:42:31
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : In.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\filter\fields;

use quick\admin\form\fields\Checkbox;
use quick\admin\form\fields\Select;
use think\helper\Arr;
use quick\admin\filter\fields\FieldFilter;

class In extends FieldFilter
{
    protected $query = 'whereIn';

    public function condition($inputs)
    {
        $data = filter_params();
        $value = Arr::get($data, $this->requestColumn);

        if (empty($value)) {
            return false;
        }

        $this->value = $value;
        return $this->buildCondition($this->column, $this->value);
    }

    /**
     * 多选
     * @param array $options 可选参数
     * [ 'key' => 'value']
     * @return $this
     */
    public function multipleSelect(array $options)
    {
        $this->field = Select::make($this->requestColumn,$this->label)
            ->multiple()
            ->options($options);
        return $this;
    }

    /**
     * @param array $options
     * @return Checkbox
     */
    public function checkbox(array $options)
    {
        $this->field = Checkbox::make($this->requestColumn,$this->label)->options($options)->checkButton();
        return $this->field;
    }
}