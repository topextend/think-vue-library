<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:03:29
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 16:44:13
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Lt.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\filter\fields;

use think\helper\Arr;
use quick\admin\filter\fields\FieldFilter;

class Lt extends FieldFilter
{
    public function condition($inputs)
    {
        $value = Arr::get($inputs, $this->requestColumn);
        if (is_null($value) || empty($value)) {
            return false;
        }
        $this->value = $value;
        return $this->buildCondition($this->column, '<', $this->value);
    }
}