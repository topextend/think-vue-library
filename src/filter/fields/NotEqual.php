<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:03:57
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 16:47:29
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : NotEqual.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace quick\admin\filter\fields;

use think\helper\Arr;
use quick\admin\filter\fields\FieldFilter;

class NotEqual extends FieldFilter
{
    /**
     * Get query condition from filter.
     * @param array $inputs
     * @return array|mixed|null
     */
    public function condition($inputs)
    {
        if ($this->ignore) {
            return;
        }
        $value = Arr::get($inputs, $this->requestColumn);
        if (!isset($value)) {
            return;
        }
        $this->value = $value;
        return $this->buildCondition($this->column, "<>",$this->value);
    }
}