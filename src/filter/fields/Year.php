<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:04:32
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 16:51:37
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Year.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\filter\fields;

use quick\admin\form\fields\Field;
use quick\admin\form\fields\Date;
use think\helper\Arr;
use quick\admin\filter\fields\Day;

class Year extends Day
{
    protected $query = 'whereYear';

    public function condition($inputs)
    {
        $value = Arr::get($inputs, $this->requestColumn);

        if (empty($value)) {
            return false;
        }

        if(is_numeric($value)){
            $value = strlen((string)$value) > 11 ? $value/1000:$value;
            $value = date('Y',$value);
        }

        $this->value = $value;
        return $this->buildCondition($this->column, $this->value);
    }

    /**
     * 设置默认表单字段
     * @return Field
     */
    protected function defaultField()
    {
        return $this->setField(Date::make($this->requestColumn,$this->label)->year());
    }
}