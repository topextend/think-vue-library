<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:02:32
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 16:31:02
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Day.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\filter\fields;

use quick\admin\form\fields\Field;
use quick\admin\form\fields\Date;
use think\helper\Arr;
use quick\admin\filter\fields\FieldFilter;

class Day extends FieldFilter
{
    protected $query = 'whereDay';

    public function condition($inputs)
    {
        $value = Arr::get($inputs, $this->requestColumn);

        if (empty($value)) {
            return false;
        }

        if(is_numeric($value)){
            $value = strlen((string)$value) > 11 ? $value/1000:$value;
            $value = date('Y-m-d',$value);
        }

        $this->value = $value;

        return $this->buildCondition($this->column, $this->value);
    }

    /**
     * 设置默认表单字段
     * @return Field
     */
    protected function defaultField()
    {
        return $this->setField(Date::make($this->requestColumn,$this->label));
    }
}