<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 14:09:46
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 14:15:43
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : HasClass.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\metable;

trait HasClass
{
    /**
     * @var array
     */
    protected $class = [];

    /**
     * @param $name
     * @return $this
     */
    public function setClass($name)
    {
        if (is_array($name)) {
            $this->withClass($name);
        } else {
            $this->withClass([$name => true]);
        }
        return $this;
    }

    /**
     * @param array $class
     * @return $this
     */
    protected function withClass(array $class)
    {
        $this->class = array_merge($this->class, $class);
        return $this;
    }

    /**
     * @param string $key
     * @param string $default
     * @return array|mixed|string
     */
    protected function getClass($key = '', $default = '')
    {
        if (empty($key)) {
            return $this->class;
        }
        return $this->class[$key] ?? $default;
    }
}