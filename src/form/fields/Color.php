<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:06:35
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 17:28:48
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Color.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\form\fields;

use quick\admin\Element;
use think\Exception;
use quick\admin\form\fields\Field;

class Color extends Field
{
    public $component = 'form-color-field';
    
    public $default;

    /**
     * 辅助文字的颜色
     * @param string $color
     * @return $this
     */
    public function textColor(string $color)
    {
        $this->props("text-color", $color);
        return $this;
    }

    /**
     * 预定义颜色
     * @param array $colors
     * @return $this
     */
    public function predefine(array $colors)
    {
        $this->props("predefine", $colors);
        return $this;
    }

    /**
     * 支持透明度选择
     * @return $this
     */
    public function alpha()
    {
        $this->props("show-alpha", true);
        return $this;
    }

    /**
     * 写入 v-model 的颜色的格式
     * @param $format hsl|hsv|hex|rgb
     * @return $this
     */
    public function format($format)
    {
        $this->props("color-format", $format);
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), []);
    }
}