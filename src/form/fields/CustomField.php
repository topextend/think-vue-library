<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:06:41
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 17:41:26
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : CustomField.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\form\fields;

use quick\admin\form\fields\Field;

class CustomField extends Field
{
    public $component;

    /**
     * CustomField constructor.
     * @param $column
     * @param $title
     */
    public function __construct($component,$column = '',$title = '')
    {
        $this->component = $component;
        $this->column = $column;
        $this->title = $title ?: $column;
        $this->init();
    }

    /**
     * Prepare the field for JSON serialization.
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(),[]);
    }
}