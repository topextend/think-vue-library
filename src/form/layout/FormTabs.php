<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 16:59:26
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 17:21:27
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : FormTabs.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\form\layout;

use quick\admin\components\Component;
use quick\admin\components\QuickTabs;
use quick\admin\form\fields\Field;
use quick\admin\form\Form;

class FormTabs extends QuickTabs
{
    /**
     * @param $title
     * @param string $content
     * @param string $key
     * @return $this|QuickTabs
     */
    public function tab($title, $content = '', string $key = '')
    {
        $key = $key ?: (string)count($this->panes);
        $fieldColumns = [];
        if ($content instanceof \Closure) {
            $content = \Closure::bind($content, $this);
            $form = Form::make("form");
            call_user_func($content, $form);
            $fields = $form->getFilterFields();
            /** @var Field $field */
            foreach ($fields as $field){
                $fieldColumns[] = $field->getColumn();
            }
            $content = Component::custom("div")->children($form->getFields());
        }

        $pane = Component::tabsPane($key, $title, $content);
        $pane->props('fields' , $fieldColumns);
        $this->panes = array_merge($this->panes, [$pane]);
        return $this;
    }

    /**
     * 子组件存在表单显示步骤按钮
     * @return FormTabs
     */
    public function showStep()
    {
        return $this->props('showStep',true);
    }

    /**
     * @return array
     */
    public function getChildrenComponents(): array
    {
        return array_merge(parent::getChildrenComponents(), $this->panes);
    }
}