<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 13:50:36
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 13:54:29
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : AuthInterface.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\contracts;

use think\Request;

interface AuthInterface
{
    /**
     * @param $username
     * @param $password
     * @param int $keepTime
     * @return mixed
     */
    public function login($username, $password, $keepTime = 0);

    /**
     * @return mixed
     */
    public function logout();

    /**
     * @param string $node
     * @return mixed
     */
    public function check(string $node);

    /**
     * @return mixed
     */
    public function getUserInfo();

    /**
     * @param Object $object
     * @param string $action
     * @param Request $request
     * @return mixed
     */
    public function checkAuth(Object $object, string $action, Request $request);
}