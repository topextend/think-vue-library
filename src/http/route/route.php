<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 15:06:03
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 15:07:43
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : route.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);

use quick\admin\http\middleware\DispatchAssetsQuickEvent;
use quick\admin\Quick;
use think\facade\Route;
use quick\admin\http\middleware\BootToolsMiddleware;
use quick\admin\http\middleware\AppModuleRunMiddleware;
use quick\admin\http\middleware\DispatchServingQuickEvent;

Route::get('captcha/[:config]', '\\think\\captcha\\CaptchaController@index');

Route::group('quick', function () {
    Route::get("style/<name>", function () {
        return Quick::loadStyles();
    });

    Route::get("script/<name>", function () {
        return Quick::loadScript();
    });

})->middleware([
    DispatchServingQuickEvent::class,
    DispatchAssetsQuickEvent::class,
    AppModuleRunMiddleware::class,
]);

Route::group('resource', function () {
    Route::rule('<resource>/<action>/<func?>', '\quick\admin\http\controller\ResourceController::execute');
})->middleware(array_merge([
    AppModuleRunMiddleware::class,
    BootToolsMiddleware::class,
    DispatchServingQuickEvent::class
],config('quick.middleware',[])));