<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-04 15:07:06
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-01-04 15:39:12
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : BootToolsMiddleware.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace quick\admin\http\middleware;

use quick\admin\events\ServingQuick;
use quick\admin\Quick;

class BootToolsMiddleware
{
    /**
     * @param $request
     * @param $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        Quick::bootTools($request);
        return $next($request);
    }
}